# split

This program splits the input file `f` into several output files containing `n`
lines with prefixed enumaration. Several implementations are provided.

Example usage:
python split.py -f faust.txt -n 1000
